let add = document.getElementById("add");
const pause = document.getElementById("start-pause");
const reset = document.getElementById("reset");
let minutes = document.getElementById("minutes");
let seconds = document.getElementById("seconds");
const addedTime = document.getElementById("added-time");

let setMinutes = 00;
let setSeconds = seconds.innerHTML;

pause.innerHTML = "Start";

let interval;
let timeArr = [];
let id = 0;

reset.addEventListener("click", function () {
  localStorage.setItem("savedMinutes", "00");
  localStorage.setItem("savedSeconds", "00");
  localStorage.setItem("savedHtml", null);

  location.reload();
});

document.addEventListener("DOMContentLoaded", function () {
  let savedMinutes = localStorage.getItem("savedMinutes");
  let savedSeconds = localStorage.getItem("savedSeconds");
  let savedHtml = localStorage.getItem("savedHtml");
  let savedHtmlArr = savedHtml.split(',');

  minutes.innerHTML = savedMinutes;
  seconds.innerHTML = savedSeconds;
  setMinutes = savedMinutes;
  setSeconds = savedSeconds;

  if (savedHtml !== 'null') {
    for (let item of savedHtmlArr){
      let timeWrapper = document.createElement("div");
    timeWrapper.classList.add("time-wrapper");

    let timeDiv = document.createElement("div");
    timeDiv.classList.add("time");
    timeDiv.textContent = `${item}`;

    let close = document.createElement("div");
    close.classList.add("close");
    close.textContent = "X";

    timeWrapper.append(timeDiv);
    timeWrapper.append(close);
    addedTime.append(timeWrapper);
    }
  }
});

add.addEventListener("click", function () {
  let timeWrapper = document.createElement("div");
  timeWrapper.classList.add("time-wrapper");

  let timeDiv = document.createElement("div");
  timeDiv.classList.add("time");
  timeDiv.textContent = `${setMinutes}:${setSeconds}`;

  let close = document.createElement("div");
  close.classList.add(`close`);
  close.setAttribute('id', ++id);
  close.textContent = "X";

  timeWrapper.append(timeDiv);
  timeWrapper.append(close);
  addedTime.append(timeWrapper);
  timeArr.push(timeDiv.innerHTML);
  localStorage.setItem("savedHtml", timeArr);

});
pause.addEventListener("click", function () {
  if (pause.textContent === "Start") {
    pause.innerHTML = "Pause";
    interval = setInterval(() => {
      if (setSeconds == 60) {
        setMinutes += 1;
        minutes.innerHTML = setMinutes;
        setSeconds = 0;
      }
      setSeconds++;
      seconds.innerHTML = setSeconds;
    }, 1000);
  } else if (pause.textContent === "Pause") {
    clearInterval(interval);
    localStorage.setItem("savedMinutes", setMinutes);
    localStorage.setItem("savedSeconds", setSeconds);

    pause.innerHTML = "Start";
  }
});